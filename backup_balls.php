CModule::IncludeModule('iblock');
$arTransactionTypes = array();
$rsType = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => 36, 'CODE' => 'TRANSACTION_TYPE'));
while($arType = $rsType->Fetch()){
	$arTransactionTypesID[$arType['ID']] = $arType['XML_ID'];
}

$rsElement = CIBlockElement::GetList(
	array('DATE_CREATED' => 'ASC'),
	array(
		'IBLOCK_ID' => 36,
		'ACTIVE' => 'Y',
	),
	false,
	false,
	array(
		'ID',
		'PROPERTY_TRNSACTION_SUMM',
		'PROPERTY_TRANSACTION_TYPE',
		'PROPERTY_BUYER_MAIL',
		'PROPERTY_BUYER_CARD',
	)
);

$arBalls = array();
if($rsElement->SelectedRowsCount() > 0){
	while($arItem = $rsElement->Fetch()){

		$summ = intval($arItem['PROPERTY_TRNSACTION_SUMM_VALUE']);
		$type = $arTransactionTypesID[$arItem['PROPERTY_TRANSACTION_TYPE_ENUM_ID']];
		$card = $arItem['PROPERTY_BUYER_CARD_VALUE'];
		
		if($type == 'write_off' && $summ > 0){
			$arBalls[$card] -= $summ;
		}else{
			$arBalls[$card] += $summ;
		}
	}

}
