<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);
define("GDS_LOG_FILENAME", $_SERVER['DOCUMENT_ROOT'].'/transaction/log.txt');

function convertArray(&$value){
	if(is_array($value)){
		foreach($value as $k => &$v){
			convertArray($v);
		}
	}else{
		$value = iconv ('cp1251', 'UTF-8', $value);
	}
	return $value;
}


function convertArrayToCp1251(&$value){
	if(is_array($value)){
		foreach($value as $k => &$v){
			convertArrayToCp1251($v);
		}
	}else{
		$value = iconv ( 'UTF-8', 'cp1251', $value);
	}
	return $value;
}


function includeClasses($path = __DIR__, $arExcludeFiles = array()){
	$handle = @opendir($path);
	if ($handle)
	{
		while ($file = readdir($handle))
		{
			if (in_array($file, array(".", "..")))
				continue;
			if (in_array($file, $arExcludeFiles)) //файлы и каталоги которые следует исключить
				continue;
			if(strpos($file, '.class.php')){
				include_once($path.'/'.$file);
			}
		}
	}
}


function GDSAddMessage2Log($sText, $sModule = "", $traceDepth = 6, $bShowArgs = false)
{
    if (defined("GDS_LOG_FILENAME") && strlen(GDS_LOG_FILENAME)>0)
    {
        if(!is_string($sText))
        {
            $sText = var_export($sText, true);
        }
        if (strlen($sText)>0)
        {
            ignore_user_abort(true);
            if ($fp = @fopen(GDS_LOG_FILENAME, "ab"))
            {
                if (flock($fp, LOCK_EX))
                {
					$sText = utf8win1251($sText);
                    @fwrite($fp, "Host: ".$_SERVER["HTTP_HOST"]."\nDate: ".date("Y-m-d H:i:s")."\nModule: ".$sModule."\n".$sText."\n");

                    @fwrite($fp, "----------\n");
                    @fflush($fp);
                    @flock($fp, LOCK_UN);
                    @fclose($fp);
                }
            }
            ignore_user_abort(false);
        }
    }
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
includeClasses(__DIR__.'/classes/');

if($_GET['test'] == 'Y'){

	$_GET += array(
		'CODE_MAGAZ' => 'mir_uvlecheniy',
		'NUMBER_CART'  => '112345',
		'EMAIL' =>  'goldenplan@yandex.ru',
		'HASH' => '36f640bd3994aeeb538de7856b775375',
		'SUMMA' => 120,
		'TIME' => 1449477699,
		'DOC_TYPE' => 'check',
		'DOC_NUMBER' => '213123',
		'DOC_DATE' => '27.12.2015 12:28:01',
	);
	/*
	$_GET += array(
		'CODE_MAGAZ' => 'mir_uvlecheniy',
	    'EMAIL' => 's_belsky@bestgarden.ru',
	    'NUMBER_CART' => '300000',
	    'SUMMA' => 236,
	    'DOC_TYPE' => 'order',
	    'DOC_NUMBER' => 'ИНТ-90334',
	    'DOC_DATE' => '13.01.2016 11:41:43',
	    'TIME' => 1452674503,
	    'HASH' => '12c3cf778c4f4b7c1318eca054b66d13',
	);
	*/
}

// GDSAddMessage2Log($_GET, 'step 1');
