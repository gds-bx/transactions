<?
class CGDSApi
{
	private $_POST = array();
	private $_GET = array();
	private $arCheckFields = array('CODE_MAGAZ', 'NUMBER_CART', 'EMAIL', 'TIME', 'HASH');

	private $arIblock = array(
		'points' => 9
	);
	/*
	SUMMA - integer
	NUMBER_CART - string
	EMAIL - string
	DOC_TYPE -  На данный момент может принимать значения  "order" или "check"
	DOC_NUMBER - string
	TIME - interger (timestamp)
	HASH - string

	Для проверки проверка:
	CODE_MAGAZ - string
	HASH - string = md5(NUMBER_CART + CODE_MAGAZ + EMAIL + TIME + PASS)
	*/
	function __construct(){
		CGDSResponse::setDefaultLang();

		$this->_POST = array_map('trim', $_POST);
		$this->_GET = array_map('trim', $_GET);
		$this->arData = $this->_GET;

		$this->obTransaction = new CGDSTransaction();
		$this->obTransaction->setData($this->arData);
	}

	function __call($func, $params){
		CGDSResponse::error('NOT_FUNCTION_CALLED');
	}

	public function callMethod($method = ''){
		$method = htmlspecialchars($method);
		if(strlen($method) < 1){
			$method = 'not_method';
		}

		$this->$method();
	}

	private function report(){
		$this->arCheckFields = array('NUMBER_CART', "HASH", "TIME");
		$this->checkFields();
		
		$hash = ToLower(md5($this->arData['NUMBER_CART'].$this->arData['TIME'].$this->getSecret())); //$this->arData['EMAIL']

		if($hash != $this->arData['HASH']){
			CGDSResponse::error('INVALID_HASH', array('#HASH#' => $_GET['test']=='Y' ? ' '.$hash : ''));
		}
		
		$this->obTransaction->report();
		
	}
	
	private function getSecret(){
		return \Bitrix\Main\Config\Option::get('gds.transactions', "SECRET_WORD");
	}
	
	private function add(){
		$this->addCheckFields(array('SUMMA', 'DOC_TYPE', 'DOC_NUMBER', 'DOC_DATE'));
		$this->security();
		$this->obTransaction->add();
	}

	private function written(){
		$this->addCheckFields(array('SUMMA', 'DOC_TYPE', 'DOC_NUMBER', 'DOC_DATE'));
		$this->security();

		$this->obTransaction->written($this->arData);
	}

	private function getList(){

		$this->security();

		$this->obTransaction->getList();
	}
	/*private function correction(){

	}
	*/
	private function balance(){

		$this->security();

		$this->obTransaction->getBalance($this->arData);
	}

	private function checkcard(){
		$this->security();

		$obCart = $this->obTransaction->getObjectCarts();

		$carResult = $obCart->check();
		if(!$carResult['status']){
			CGDSResponse::error($carResult['message']);
		}else{
			CGDSResponse::succes($carResult['message']);
		}
	}

	private function cancel(){

	}

	private function security(){
		$this->checkFields();

		$pass = $this->getPassword();

		$hash = ToLower(md5($this->arData['NUMBER_CART'].$this->arData['CODE_MAGAZ'].$this->arData['TIME'].$pass)); //$this->arData['EMAIL']

		if($hash != $this->arData['HASH']){
			CGDSResponse::error('INVALID_HASH', array('#HASH#' => $_GET['test']=='Y' ? ' '.$hash : ''));
			//throw new ExceptionTransaction('invalid hash '.$hash);
		}
	}

	public function getPassword($code){

		$obShop = CGDSShop::getInstance();
		$obShop->setCode($this->arData['CODE_MAGAZ']);
		$arShop = $obShop->getShop();

		$this->arData['SHOP_ID'] = $arShop['ID'];

		$lang = trim($arShop['PROPERTIES']['LANG']['VALUE']);
		if(strlen($lang) > 0){
			CGDSResponse::setLang($lang);
		}


		return $arShop['PROPERTIES']['MAG_PASS']['VALUE'];
	}

	function checkFields(){
		$this->arCheckFields = array_map('trim', $this->arCheckFields);

		$arErrorCode = array();
		foreach($this->arCheckFields as $code){
			if(strlen($this->arData[$code]) < 1){
				$arErrorCode[] = $code;
				//throw new ExceptionTransaction('not '.$code);
			}
		}

		if(in_array('EMAIL', $this->arCheckFields) && strlen($this->arData['EMAIL']) > 0){
			if(!check_email($this->arData['EMAIL'])){
				CGDSResponse::error('INVALID_FORMAT_EMAIL');
			}
		}

		if(in_array('DOC_DATE', $this->arCheckFields) && strlen($this->arData['DOC_DATE']) > 0){
			$time = MakeTimeStamp($this->arData['DOC_DATE'], "DD.MM.YYYY HH:MI:SS");
			$arDate = array(date("d.m.Y H:i:s", $time), date("d.m.Y G:i:s", $time));


			if(!in_array($this->arData['DOC_DATE'], $arDate)){
				CGDSResponse::error('INVALID_FORMAT_DOC_DATE');
			}
		}
		if(count($arErrorCode) > 0){
			CGDSResponse::error('NOT_CODE', array('#CODE#' => implode($arErrorCode, ', ')));
		}
	}

	function addCheckFields(array $arFields){
		$this->arCheckFields = array_merge($this->arCheckFields, $arFields);
	}
}
