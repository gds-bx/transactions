<?
class CGDSCarts
{
	//private $iblockID = 38; legacy bestgarden 
	private $arData = array();

	function __construct($arData){
		
		$this->arData = $arData;
		if(empty($this->arData)){
			//CGDSResponse::error('CART_NOT_EMAIL');
		}
	}

	function check(){
		$card = $this->arData['CART'] ? : $this->arData['NUMBER_CART'];
		
		$db = CUser::GetList($b, $o, array("LOGIN" => $this->arData["EMAIL"], "UF_NUMBER_CART" => $card), array("FIELDS" => array("ID"), "SELECT" => array("UF_NUMBER_CART")));
		
		if(!$db->SelectedRowsCount()){
			return array('status' => false, 'message' => 'CART_NOT_FOUND');
		}

		return array('status' => true, 'message' => 'CART_CHECK_OK');
	}

	function addCart(){

		$arLoadProductArray = array(
			'NAME' => $this->arData['CART'],
			'IBLOCK_ID' => $this->iblockID,
			'PROPERTY_VALUES' => array(
				'E_MAIL' => $this->arData['EMAIL']
			)
		);

		$obElement = new CIBlockElement;
		if($PRODUCT_ID = $obElement->Add($arLoadProductArray)){
			return array('status' => true, 'message' => 'CART_ADD_OK');
		}else{
			return array('status' => false, 'message' => $obElement->LAST_ERROR);
		}
	}
}
