<?
class CGDSShop
{
	private $iblockId = 9;
	private $code = null;
	private $arShop = array();
	static private $object = null;
	private $obError = null;

	function __construct(){
		if(!CModule::IncludeModule('iblock')){
			CGDSResponse::error('NOT_MODULE_IBLOCK');
		}
	}

	function getShop(){

		if(!array_key_exists($this->code, $this->arShops)){
			$rsShop = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $this->iblockId, 'PROPERTY_MAG_ID' => $this->code));
			if($resShop = $rsShop->GetNextElement()){

				$arShop = $resShop->GetFields();
				$arShop['PROPERTIES'] = $resShop->GetProperties();

				$this->arShops[$this->code] = $arShop;
			}
		}
		if(!array_key_exists($this->code, $this->arShops) || empty($this->arShops[$this->code])){
			CGDSResponse::error('NOT_FOUND_SHOP');
		}

		return $this->arShops[$this->code];
	}
	
	function getList(){
		$filter = array(
			"IBLOCK_ID" => $this->iblockId,
			"!PROPERTY_MAG_ID" => false,
			"ACTIVE" => "Y"
		);
		$list = array();
		$db = CIBlockElement::GetList(array(), $filter);
		while($dbObject = $db->GetNextElement()){

			$arShop = $dbObject->GetFields();
			$arShop['PROPERTIES'] = $dbObject->GetProperties();
			
			$list[$arShop["ID"]] = $arShop;
		}
		
		return $list;
	}

	function setCode($code){
		$this->code = $code;
	}

	function getInstance(){
		if(self::$object == null)
		{
			self::$object = new CGDSShop();
		}

		return self::$object;
	}
}
