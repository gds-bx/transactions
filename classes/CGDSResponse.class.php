<?
class CGDSResponse
{
	static private $status = '';
	static private $message = '';
	static private $errorCode = '0';
	static private $lang = 'RU';

	public function succes($message, $params){
		self::$status = 'ok';
		if(!is_array($message)){
			$message = GetMessage($message, $params);
		}else{
			$message = convertArray($message);
		}

		self::$message = $message;
		self::show();
	}

	public function error($code, $params = array()){
		self::$status = 'error';
		self::$message = GetMessage($code, $params);
		self::$errorCode = $code;
		self::show();
	}

	static private function show(){
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		$result = array(
			'status' => self::$status,
			'message' => self::$message,
			'error_code' => self::$errorCode
		);

		if($_GET['type'] == 'array'){
			echo '<pre>'.print_r($result ,true).'</pre>';
		}else{
			echo json_encode($result);
		}
		//header('Content-type: text/html; charset=utf-8');
		die();
	}

	static function setLang($lang){
		$lang = ToLower($lang);

		self::$lang = $lang;
		Bitrix\Main\Localization\Loc::setCurrentLang($lang);
		Bitrix\Main\Localization\Loc::loadLanguageFile(__DIR__.'/../index.php');
	}

	static function setDefaultLang(){
		self::setLang(self::$lang);
	}
}
