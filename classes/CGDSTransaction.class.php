<?
class CGDSTransaction
{
	private $arIblock = array(
		'points' => 9,
		'transaction' => 8
	);
	const DOC_TYPE_ORDER = '4';
	const DOC_TYPE_CHECK = '3';
	const DOC_TYPE_SOCIAL = '5';

	const T_TYPE_ADD = '6';
	const T_TYPE_WRITE = '9';
	const T_TYPE_CORRECT = '7';
	const T_TYPE_REPL = '8';

	function __construct(){
		if(!CModule::IncludeModule('iblock')){
			CGDSResponse::error('NOT_MODULE_IBLOCK');
		}
	}

	public function add($arFields){
	   $this->type = self::T_TYPE_ADD;
	   $this->addTransaction('TRASNSACTION_ADD_OK');
   }

   public function written($arFields){
	   $this->type = self::T_TYPE_WRITE;
	   $this->arData['SUMMA'] = 0 - $this->arData['SUMMA'];
	   $this->addTransaction('TRASNSACTION_WRITEN_OK');
   }

	private function addTransaction($okMessage = ''){
		if(empty($this->type)){
			CGDSResponse::error('TRANSACTION_NOT_TYPE');
		}
		
		$this->addCart();

		CModule::IncludeModule('iblock');

		$arFields = $this->arData;
		$el = new CIBlockElement;

		if ($this->type == self::T_TYPE_WRITE) {
			$balance = $this->getBalance(true);
			$diff = $balance + intval($arFields['SUMMA']);
			if ($diff < 0) {
				CGDSResponse::error('NO_MORE_POINTS');
			}
		}
		
		$arShop = CGDSShop::getInstance()->getShop();

		$date =  ConvertTimeStamp(time()+CTimeZone::GetOffset(), "FULL");
		$arFields = $this->arData;
		$arTransaction = array(
			'NAME' => 'Transaction',
			'IBLOCK_ID' => $this->arIblock['transaction'],
			'PROPERTY_VALUES' => array(
				'TRNSACTION_SUMM' => $arFields['SUMMA'],
				'BUYER_MAIL' => $arFields['EMAIL'],
				'BUYER_CARD' => $arFields['NUMBER_CART'],
				'TRANSACTION_TYPE' => $this->type,
				'OSNOVANIE' => $this->getDocType($arFields['DOC_TYPE']),
				'DOC_NUMBER' => iconv("UTF-8", "windows-1251", (string) $arFields['DOC_NUMBER']),
				'DOC_DATE' => (string) $arFields['DOC_DATE'],
				'MAGAZINE' => (int) $arShop['ID'],
			)
		);
		
		if ($arShop['PROPERTIES']['ALLOW_DUPLICATION']['VALUE_XML_ID'] != 'yes') {
			$day = MakeTimeStamp($arFields['DOC_DATE']);
			$dayNext = AddToTimeStamp(array('DD' => 1), $day);

			$arFilter = array(
				'IBLOCK_ID' => $this->arIblock['transaction'],
				'PROPERTY_DOC_NUMBER' =>  iconv("UTF-8", "windows-1251", (string) $arFields['DOC_NUMBER']),
				'PROPERTY_TRANSACTION_TYPE' =>  $this->type,
				'PROPERTY_OSNOVANIE' => $this->getDocType($arFields['DOC_TYPE']),
				"><PROPERTY_DOC_DATE" => array(date("Y-m-d", $day), date("Y-m-d", $dayNext))
			);
			
			if($ar = CIBlockElement::GetList(array(), $arFilter)->Fetch()){
				CGDSResponse::error('TRANSACTION_DOUBLE', array('#ID#' => $ar['ID']));
			}

			$arTransaction['DATE_ACTIVE_FROM'] = $date;
			$arTransaction['PROPERTY_VALUES']['TRNSACTION_DATE'] = $date;
		}
		
		$result = $el->Add($arTransaction);
		if($result){
			CGDSResponse::succes($okMessage);
		}else{
			CGDSResponse::error('SAVE_ERROR', array('#ERROR' => $el->LAST_ERROR));
		}
	}

	function getBalance($isReturn = false){

		$this->checkCart();

		$arInfo = $this->getTransactions();
		$arTransactionTypes = $arInfo['enums']['TRANSACTION_TYPE'];

		$summAll = 0;
		$rsElement = $arInfo['result'];
		while($arItem = $rsElement->Fetch()){

			$summ = intval($arItem['PROPERTY_TRNSACTION_SUMM_VALUE']);
			$type = $arTransactionTypes[$arItem['PROPERTY_TRANSACTION_TYPE_ENUM_ID']];

			if($type == 'write_off' && $summ > 0){
				$summAll -= $summ;
			}else{
				$summAll += $summ;
			}
		}
		
		if ($isReturn) {
			return $summAll;
		}

		CGDSResponse::succes('CART_BALANCE', array('#SUMM#' => $summAll));
	}
	
	function report(){
		$this->checkCart();

		$arInfo = $this->getTransactions();
		$arTransactionTypes = $arInfo['enums']['TRANSACTION_TYPE'];
		$arDocTypes = $arInfo['enums']['OSNOVANIE'];

		$arShops = CGDSShop::getInstance()->getList();

		$rsElement = $arInfo['result'];

		$arList = array();
		while($resItem = $rsElement->getNextElement()){
			$arItem = $resItem->GetFields();
			$arItem['PROPERTIES'] = $resItem->GetProperties();
			$shop = $arShops[$arItem["PROPERTY_MAGAZINE_VALUE"]];
			$transaction = array(
				'ID' => $arItem['ID'],
				'TIME' => $arItem['PROPERTY_TRNSACTION_DATE_VALUE'], //??
				'CODE_MAGAZINE' => $shop['NAME'].'['.$shop['PROPERTIES']['MAG_ID']['VALUE'].']', //??
				'TYPE' => $arItem['PROPERTY_TRANSACTION_TYPE_VALUE'],
				'TYPE_CODE' => $arTransactionTypes[$arItem['PROPERTY_TRANSACTION_TYPE_ENUM_ID']],
				'SUMMA' => $arItem['PROPERTY_TRNSACTION_SUMM_VALUE'],
				'NUMBER_CART' => $arItem['PROPERTY_BUYER_CARD_VALUE'],
				'EMAIL' => $arItem['PROPERTY_BUYER_MAIL_VALUE'],
				'DOC_TYPE' => $arItem['PROPERTY_OSNOVANIE_VALUE'],
				'DOC_TYPE_CODE' => $arDocTypes[$arItem['PROPERTY_OSNOVANIE_ENUM_ID']],
				'DOC_NUMBER' => $arItem['PROPERTY_DOC_NUMBER_VALUE'],
				'DOC_DATE' => $arItem['PROPERTY_DOC_DATE_VALUE'],

			);

			$transaction['REPLACE_CARD'] = $arItem['PROPERTY_REPLACE_CARD_VALUE'];

			if($this->arData['NUMBER_CART'] == 112345){
				//echo "<pre>".print_r($arItem, true)."</pre>";die();
			}
			$arList[] = $transaction;
		}

		if(count($arList) < 1){
			CGDSResponse::error('TRANSACTION_NOT_FOUND');
		}

		CGDSResponse::succes($arList);
	}

	function getList(){
		$this->checkCart();

		$arInfo = $this->getTransactions();
		$arTransactionTypes = $arInfo['enums']['TRANSACTION_TYPE'];
		$arDocTypes = $arInfo['enums']['OSNOVANIE'];

		$arShop = CGDSShop::getInstance()->getShop();

		$rsElement = $arInfo['result'];

		$arList = array();
		while($resItem = $rsElement->GetNextElement()){
			$arItem = $resItem->GetFields();
			$arItem['PROPERTIES'] = $resItem->GetProperties();
			$transaction = array(
				'ID' => $arItem['ID'],
				'TIME' => $arItem['PROPERTY_TRNSACTION_DATE_VALUE'], //??
				'CODE_MAGAZINE' => $arShop['NAME'].'['.$arShop['PROPERTIES']['MAG_ID']['VALUE'].']', //??
				'TYPE' => $arItem['PROPERTY_TRANSACTION_TYPE_VALUE'],
				'TYPE_CODE' => $arTransactionTypes[$arItem['PROPERTY_TRANSACTION_TYPE_ENUM_ID']],
				'SUMMA' => $arItem['PROPERTY_TRNSACTION_SUMM_VALUE'],
				'NUMBER_CART' => $arItem['PROPERTY_BUYER_CARD_VALUE'],
				'EMAIL' => $arItem['PROPERTY_BUYER_MAIL_VALUE'],
				'DOC_TYPE' => $arItem['PROPERTY_OSNOVANIE_VALUE'],
				'DOC_TYPE_CODE' => $arDocTypes[$arItem['PROPERTY_OSNOVANIE_ENUM_ID']],
				'DOC_NUMBER' => $arItem['PROPERTY_DOC_NUMBER_VALUE'],
				'DOC_DATE' => $arItem['PROPERTY_DOC_DATE_VALUE'],

			);

			$transaction['REPLACE_CARD'] = $arItem['PROPERTY_REPLACE_CARD_VALUE'];

			if($this->arData['NUMBER_CART'] == 112345){
				//echo "<pre>".print_r($arItem, true)."</pre>";die();
			}
			$arList[] = $transaction;
		}

		if(count($arList) < 1){
			CGDSResponse::error('TRANSACTION_NOT_FOUND');
		}

		CGDSResponse::succes($arList);
	}

	function getTransactions(){
		$arEnums = array();
		$rsEnums = CIBlockPropertyEnum::GetList(
			array(),
			array(
				'IBLOCK_ID' => $this->arIblock['transaction']
				//,'CODE' => 'TRANSACTION_TYPE'
			)
		);
		while($arEnum = $rsEnums->Fetch()){
			$arEnums[$arEnum['PROPERTY_CODE']][$arEnum['ID']] = $arEnum['XML_ID'];
		}

		$rsElement = CIBlockElement::GetList(
			array('PROPERTY_TRNSACTION_DATE' => 'DESC'),
			array(
				'IBLOCK_ID' => $this->arIblock['transaction'],
				'ACTIVE' => 'Y',
				'=PROPERTY_BUYER_CARD' => $this->arData['NUMBER_CART'],
				'=PROPERTY_BUYER_MAIL' => $this->arData['EMAIL']
			),
			false,
			false,
			array(
				'ID',
				'PROPERTY_TRNSACTION_SUMM',
				'PROPERTY_TRANSACTION_TYPE',
				'PROPERTY_BUYER_MAIL',
				'PROPERTY_MAGAZINE',
				'PROPERTY_TRNSACTION_DATE',
				'PROPERTY_BUYER_CARD',
				'PROPERTY_BUYER_MAIL',
				'PROPERTY_OSNOVANIE',
				'PROPERTY_DOC_NUMBER',
				'PROPERTY_DOC_DATE',
				'PROPERTY_REPLACE_CARD'
			)
		);

		if($rsElement->SelectedRowsCount() < 1){
			CGDSResponse::error('TRANSACTION_NOT_FOUND');
		}

		return array(
			'result' => $rsElement,
			'enums' => $arEnums
		);
	}

	function getDocType($type = ''){
		switch($type){
			case "check":
				return self::DOC_TYPE_CHECK;
			case "social":
				return self::DOC_TYPE_SOCIAL;
			default:
				return self::DOC_TYPE_ORDER;
		}
	}

	function getObjectCarts(){
		if(empty($this->obCarts))
		{
			$this->obCarts = $this->loadCarts();
		}

		return $this->obCarts;
	}

	private function loadCarts(){
		$arCart = array(
			'CART' => $this->arData['NUMBER_CART'],
			'EMAIL' => $this->arData['EMAIL']
		);

		return new CGDSCarts($arCart);
	}

	function getObjectShop(){
		if(empty($this->obShops))
		{
			$this->obShops = new CGDSShop();
		}

		return $this->obShops;
	}

	function checkCart(){

		$obCart = $this->getObjectCarts();
		$carResult = $obCart->check();
		
		if(!$carResult['status']){
/*TODO*/	CGDSResponse::error($carResult['message']);
		}
	}

	function addCart(){
		$obCart = $this->getObjectCarts();
		
		$carResult = $obCart->check();
		if(!$carResult['status']){
			CGDSResponse::error($carResult['message']);
		}
	}

	function setData($arFields){
		$this->arData = array_map('trim', $arFields);
	}
}
