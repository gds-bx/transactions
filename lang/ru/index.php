<?
$MESS['NOT_MODULE_IBLOCK'] = 'Модуль "инфоблок" не найден';
//api
$MESS['NOT_FUNCTION_CALLED'] = 'Функция не найдена';
$MESS['INVALID_HASH'] = 'неправильный HASH#HASH#';
$MESS['NOT_CODE'] = 'Не найден: #CODE#';
$MESS['INVALID_FORMAT_EMAIL'] = 'EMAIL неверный формат';
$MESS['INVALID_FORMAT_DOC_DATE'] = 'DOC_DATE неверный формат';
//cart
$MESS['CART_NOT_EMAIL'] = 'Карта привязана на другой EMAIL';
$MESS['CART_NOT_FOUND'] = 'Карта не найдена';
$MESS['CART_CHECK_OK'] = 'Карта найдена';
$MESS['CART_ADD_OK'] = 'Карта добавлена';
$MESS['CART_BALANCE'] = '#SUMM#';
//shop
$MESS['NOT_FOUND_SHOP'] = 'магазин не найден';
//transaction
$MESS['TRASNSACTION_ADD_OK'] = 'Транзакция успешно создана. Балы зачислены.';
$MESS['TRASNSACTION_WRITEN_OK'] = 'Транзакция успешно создана. Баллы списаны.';
$MESS['TRANSACTION_NOT_TYPE'] = 'Нет типа транзакции';
$MESS['TRANSACTION_DOUBLE'] = 'Дублирование транзакции  №#ID#';
$MESS['SAVE_ERROR'] = 'Ошибка при сохранении: #ERROR#';
$MESS['TRANSACTION_NOT_FOUND'] = 'Транзакции не найдены';
$MESS['NO_MORE_POINTS'] = 'Недостаточно баллов на счету';
