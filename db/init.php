<?

AddEventHandler('main', 'OnBeforeEventSend', Array("UserEvents", "my_OnBeforeEventSend"));
//AddEventHandler("main", "OnSendUserInfo", array('UserEvents', "OnSendUserInfo"));
AddEventHandler("main","OnBeforeUserRegister", array('UserEvents', "UpdateUserRegister"));
AddEventHandler("main", "OnBeforeUserLogin", array('UserEvents', "OnBeforeUserLoginHendler"));
AddEventHandler("main", "OnBeforeUserSendPassword", array('UserEvents', "CustomOnBeforeUserSendPassword"));

Class UserEvents{
	static $active = 'Y';

	function CustomOnBeforeUserSendPassword($arFields) {
		if(!empty($arFields["LOGIN"])){
			$arResultUser = CUser::GetByLogin($arFields["LOGIN"])->Fetch();
		}
		if($arResultUser["ACTIVE"] != "Y"){
			self::$active = 'N';
			global $USER;
			$USER->Update($arResultUser["ID"], array("ACTIVE" => "Y"));
		}
	}

	function my_OnBeforeEventSend(&$arFields, &$arTemplate) {
		$arMailEvents = array("NEW_USER_CONFIRM", "NEW_USER", "USER_PASS_REQUEST");
		if(in_array($arTemplate["EVENT_NAME"], $arMailEvents) && !$_REQUEST["forgot_password"]){
			global $USER;
			$db = $USER->GetByLogin($arFields["LOGIN"]);
			if($arUser = $db->Fetch()){
				$newPass = randomPassword();
				$obUser = new \Bestgarden\Gds\Mobileapi\User;
				$USER->Update($arUser['ID'], array('PASSWORD' => $newPass, 'CONFIRM_PASSWORD' => $newPass, 'UF_CODE' => $obUser->encrypt($newPass)));
				$arFields['NEW_PASSWORD'] = $newPass;
			}
			//$arFields["NEW_PASSWORD"] = $_POST["PASSWORD"];
			unset($_POST["PASSWORD"]);
		}
	}

	function OnSendUserInfo(&$arFields){
		global $USER;
		if(self::$active == 'N'){
			$USER->Update($arFields['USER_FIELDS']['ID'], array("ACTIVE" => "N"));
		}
		$newPass = randomPassword();
		$USER->Update($arFields['USER_FIELDS']['ID'], array('PASSWORD' => $newPass, 'CONFIRM_PASSWORD' => $newPass));
		$arFields['FIELDS']['NEW_PASSWORD'] = $newPass;
	}

	function OnBeforeUserLoginHendler(&$arFields){
		$dbResult = CUser::GetByLogin($arFields["LOGIN"]);
		$arResult = $dbResult->Fetch();
		$salt = substr($arResult["PASSWORD"], 0, strlen($arResult["PASSWORD"]) - 32);
		$pass = $salt.md5($salt.$arFields["PASSWORD"]);
		if($arFields["LOGIN"] == $arResult["LOGIN"] && $arResult["PASSWORD"] == $pass && $arResult["ACTIVE"] == "N"){
			global $USER;
			if(!$USER->Update($arResult["ID"], Array("ACTIVE" => "Y"))){
				unset($_POST["PASSWORD"]);
			}
		}
	}

	function UpdateUserRegister(&$arFields) {
		$arFields["LOGIN"] = ToLower($arFields["EMAIL"]);
		$arFields["PASSWORD"] = $arFields["CONFIRM_PASSWORD"] = randomPassword(2);
		$_POST["PASSWORD"] = $arFields["PASSWORD"];
		$user = new \Bestgarden\Gds\Mobileapi\User;
		$arFields["UF_CODE"] = $user->encrypt($arFields["PASSWORD"]);
	}

}


function randomPassword($gid) {
	if (!is_array($gid)) {
		if ($gid > 0) {
			$gid = array($gid);
		} else {
			$gid = array();
		}
	}
	$policy = CUser::GetGroupPolicy($gid);
	$length = $policy['PASSWORD_LENGTH'];
	if ($length <= 0) {
		$length = 6;
	}
	$chars = array(
		'abcdefghijklnmopqrstuvwxyz',
		'ABCDEFGHIJKLNMOPQRSTUVWXYZ',
		'0123456789',
	);
	if ($policy['PASSWORD_PUNCTUATION'] == 'Y') {
		$chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
	}
	return randString($length+2, $chars);
}

function TransactionAgent() {
	\Bitrix\Main\Loader::includeModule('gds.mobileapi');
	$transactionCheck = new \Bestgarden\Gds\MobileApi\Transaction;
	$transactionCheck->CheckOld();
	return 'TransactionAgent();';
}

function HashAgent() {
	\Bitrix\Main\Loader::includeModule('gds.mobileapi');
	\Bestgarden\Gds\MobileApi\Api::CheckOldHash();
	return 'HashAgent();';
}

function SendNotificationsAgent() {
	\Bitrix\Main\Loader::includeModule('gds.mobileapi');
	$osAgent = new \Bestgarden\Gds\MobileApi\OneSignal;
	$osAgent->CheckMessages();
	return 'SendNotificationsAgent();';
}
